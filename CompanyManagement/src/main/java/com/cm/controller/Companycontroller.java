package com.cm.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cm.domain.Company;

import com.cm.service.Companyservice;


@RequestMapping("/api")
@RestController
public class Companycontroller {

	@Autowired
	Companyservice companyservice;

	@GetMapping("/company")
	public List<Company> showmeApplicationName() {
		return companyservice.getall();
	}

	@PutMapping("/company/{id}")
	public Company update(@RequestBody Company company, @PathVariable("id") Long id) {
		return companyservice.update(company, id);
	}

	@PostMapping("/company")
	public Company create(@RequestBody Company company) {

		return companyservice.create(company);
	}
	@DeleteMapping("/company/{id}")
		public void delete(@PathVariable("id") Long id)
		{
				companyservice.delete(id);
		}
		
	
}
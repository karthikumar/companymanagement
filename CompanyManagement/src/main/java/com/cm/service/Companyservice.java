package com.cm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cm.dao.Companydao;
import com.cm.domain.Company;


@Component
public class Companyservice {
	@Autowired 
	private Companydao companyDao;
	
	public Company create (Company cmp) {
		System.out.println("in servive -------_> "+cmp.toString());
		return companyDao.create(cmp);
	}
	public Company update(Company company,Long id) {
		return companyDao.update(company, id);
	}
	public void delete(Long id) {
		 companyDao.delete(id);
	} 
     	
	public List<Company> getall(){
		return companyDao.getall();
	}
	

}

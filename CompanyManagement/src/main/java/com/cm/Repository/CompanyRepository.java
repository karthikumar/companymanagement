package com.cm.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.cm.domain.Company;


public interface CompanyRepository extends JpaRepository<Company, Long> {
	

}

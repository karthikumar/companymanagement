package com.cm.dao;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cm.Repository.CompanyRepository;
import com.cm.domain.Company;


@Component
public class Companydao {
	List<Company> listOfStudent = new ArrayList<>();
    @Autowired
    CompanyRepository companyRepository;
	public Company create(Company cmp) {
		//System.out.println("in servive -------_> " + cmp.toString());
		//listOfStudent.add(stu);
		return companyRepository.save(cmp);
	}
	public Company update(Company company,Long id) 
	{
		Company c=companyRepository.getOne(id);
				c.setName(company.getName());
				c.setAddress(company.getAddress());
				c.setContact(company.getContact());
		return companyRepository.save(c);
	}
	public void delete(Long id)
	{
		companyRepository.deleteById(id);
				  
	}
	

	public List<Company> getall() {
		return companyRepository.findAll();
	}

}
